package com.example.assignment;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;


public class ServiceWorker<T> {
    private Handler mainHandler;
    private volatile Looper mworkerLooper;
    private volatile WorkerThreadHandler mworkerThreadHandler;


    public ServiceWorker(String name) {
        // Initialisation
        mainHandler = new Handler(Looper.getMainLooper());
        HandlerThread handlerThread = new HandlerThread(name);
        handlerThread.start();
        mworkerLooper = handlerThread.getLooper();
        mworkerThreadHandler = new WorkerThreadHandler(mworkerLooper);
    }


    // worker handler
    private final class WorkerThreadHandler extends Handler {
        public WorkerThreadHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Task T = (Task) msg.obj;
            final Task<T> headTask = T;
            T result = null;
            try {
                result = headTask.onExecute();
            } catch (Exception e) {
            } finally {
                final T finalResult = result;
                Log.i("current thread", getLooper().getThread().getName());
                mainHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        headTask.onComplete(finalResult);
                    }
                });
            }

        }
    }

    public void addTask(Task T) {
        Message message = mworkerThreadHandler.obtainMessage();
        message.obj = T;
        mworkerThreadHandler.sendMessage(message);
    }

}
